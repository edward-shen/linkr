table! {
    links (id) {
        id -> Int4,
        origin -> Text,
        dest -> Text,
        clicks -> Int4,
    }
}
